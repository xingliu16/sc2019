import numpy as np 
L = np.sort(np.random.randint(1, 20, 8))
print("L", L)

x = 7   # Target

def serchb(L, x, debug=False):
    """ 
    Use binary search to find index of x in L
    Input  : L = list of elements
             x = element to be located
    Output : imid = index of x in L
    """
    assert type(x) is int or type(x) is float, "error, x must be numeric" 
    ind = -100
    n = len(L)
    istart = 0
    iend = n-1

    # Iterate through the list
    while istart <= iend:
        imid = (iend + istart) // 2   # Median element

        if debug:
            print("istart, imid, iend:", istart, imid, iend)
        # Compare median and discard half
        if x == L[imid]:
            ind = imid
            print("ind:", ind)
            break
        elif x < L[imid]:
            iend = imid - 1
        else:
            istart = imid + 1
        
    return ind

    

     