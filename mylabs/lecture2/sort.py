 import numpy as numpy
 L = list(np.random.randint(1, 20, 8))

 n =  len(L)

# Make a copy of L
 S = L.copy()

 for i in range(1, n):
    for j in range(i-1, -1, -1):
        if S[j + 1] < S[j]:
            S[j], S[j + 1] = S[j + 1], S[j]
        else:
            break
            